﻿namespace Matrix.Services.Photos
{
    public class CloudinaryConfig
    {
        public string CloudName { get; set; }

        public string ApiKey { get; set; }

        public string ApiSecret { get; set; }
    }
}