﻿using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace Matrix.Web.ApiControllers
{
    [Route("api/activities")]
    [ApiController]
    public class ActivityController : ControllerBase
    {
        #region Fields

        private readonly IActivityService _activityService;

        #endregion

        #region Constructors

        public ActivityController(IActivityService activityService)
        {
            _activityService = activityService;
        }

        #endregion

        #region Public Methods

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] int? limit, int? offset, bool isGoing, bool isHost, DateTime? startDate)
        {
            var activities = await _activityService.Get(limit, offset, isGoing, isHost, startDate);

            return Ok(activities);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var activity = await _activityService.Get(id);

            return Ok(activity);
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] ActivityDto activityDto)
        {
            var IsCreated = await _activityService.Create(activityDto);

            return Ok(IsCreated);
        }

        [HttpPost("{id}/attend")]
        public async Task<IActionResult> Attend(Guid id)
        {
            var attendActivity = await _activityService.Attend(id);

            return Ok(attendActivity);
        }

        [HttpDelete("{id}/attend")]
        public async Task<IActionResult> UnAttend(Guid id)
        {

            var unAttendActivity = await _activityService.UnAttend(id);

            return Ok(unAttendActivity);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] ActivityDto activityDto)
        {
            activityDto.Id = id;
            var activity = await _activityService.Update(activityDto);

            return Ok(activity);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var activity = await _activityService.Delete(id);

            return Ok(activity);
        }

        #endregion
    }
}