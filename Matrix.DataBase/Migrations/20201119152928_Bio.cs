﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Matrix.DataBase.Migrations
{
    public partial class Bio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("01a15629-2f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15629-2f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15629-2f40-4db2-ac7c-b337f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15629-2f49-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15629-2f60-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15629-3f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15659-2f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03a15829-2f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("03b15629-2f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "Id",
                keyValue: new Guid("13a15629-2f40-4db2-ac7c-b335f3fdf4c3"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "5f8bd1d6-371e-4b8a-8e1e-ba2c4c73492d");

            migrationBuilder.AddColumn<string>(
                name: "Bio",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bio",
                table: "AspNetUsers");

            migrationBuilder.InsertData(
                table: "Activities",
                columns: new[] { "Id", "Category", "City", "Date", "Description", "IsDeleted", "Title", "Venue" },
                values: new object[,]
                {
                    { new Guid("03a15629-2f40-4db2-ac7c-b335f3fdf4c3"), "drinks", "London", new DateTime(2020, 9, 19, 13, 48, 48, 863, DateTimeKind.Local).AddTicks(1998), "Activity 2 months ago", false, "Past Activity 1", "Pub" },
                    { new Guid("03b15629-2f40-4db2-ac7c-b335f3fdf4c3"), "culture", "Paris", new DateTime(2020, 10, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(8842), "Activity 1 month ago", false, "Past Activity 2", "Louvre" },
                    { new Guid("01a15629-2f40-4db2-ac7c-b335f3fdf4c3"), "culture", "London", new DateTime(2020, 12, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9016), "Activity 1 month in future", false, "Future Activity 1", "Natural History Museum" },
                    { new Guid("13a15629-2f40-4db2-ac7c-b335f3fdf4c3"), "music", "London", new DateTime(2021, 1, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9040), "Activity 2 months in future", false, "Future Activity 2", "O2 Arena" },
                    { new Guid("03a15659-2f40-4db2-ac7c-b335f3fdf4c3"), "drinks", "London", new DateTime(2021, 2, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9058), "Activity 3 months in future", false, "Future Activity 3", "Another pub" },
                    { new Guid("03a15829-2f40-4db2-ac7c-b335f3fdf4c3"), "drinks", "London", new DateTime(2021, 3, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9071), "Activity 4 months in future", false, "Future Activity 4", "Yet another pub" },
                    { new Guid("03a15629-3f40-4db2-ac7c-b335f3fdf4c3"), "drinks", "London", new DateTime(2021, 4, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9086), "Activity 5 months in future", false, "Future Activity 5", "Just another pub" },
                    { new Guid("03a15629-2f49-4db2-ac7c-b335f3fdf4c3"), "music", "London", new DateTime(2021, 5, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9103), "Activity 6 months in future", false, "Future Activity 6", "Roundhouse Camden" },
                    { new Guid("03a15629-2f60-4db2-ac7c-b335f3fdf4c3"), "travel", "London", new DateTime(2021, 6, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9118), "Activity 2 months ago", false, "Future Activity 7", "Somewhere on the Thames" },
                    { new Guid("03a15629-2f40-4db2-ac7c-b337f3fdf4c3"), "film", "London", new DateTime(2021, 7, 19, 13, 48, 48, 869, DateTimeKind.Local).AddTicks(9131), "Activity 8 months in future", false, "Future Activity 8", "Cinema" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "DisplayName", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "5f8bd1d6-371e-4b8a-8e1e-ba2c4c73492d", 0, "dc3359c5-8f20-4676-b237-f96f181803ed", "Petko", "petko@matrix.com", false, false, null, null, null, "Matrix#23", null, false, "b7f7e5f4-b41f-415f-a756-ea564904f842", false, "petko" });
        }
    }
}
