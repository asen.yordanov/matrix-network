﻿using Matrix.Services.Contracts;
using Matrix.Web.ApiControllers;
using Moq;
using NUnit.Framework;
using System;

namespace Matrix.Services.Test.Controller_Tests
{
    [TestFixture]
    public class ActivityController_Should
    {
        private Mock<IActivityService> mockedActivityService;
        private ActivityController activityController;

        [SetUp]
        public void ActivityControllerShouldSetUp()
        {
            mockedActivityService = new Mock<IActivityService>();
            activityController = new ActivityController(mockedActivityService.Object);
        }

        [Test]
        public void TestConstructor()
        {
            Assert.That(activityController, Is.TypeOf<ActivityController>());
            Assert.IsNotNull(activityController);
        }

        [Test]
        public void ThrowsArgumentNullExceptionWhenFollowerServiceIsNull()
        {
            mockedActivityService = null;
            Assert.Throws<NullReferenceException>(() => new ActivityController(mockedActivityService.Object));
        }
    }
}
