﻿using BeerLab.Services.Test;
using Matrix.DataBase;
using Matrix.Models;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Matrix.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Matrix.Services.Test.FollowerService_Tests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task Return_Correct_Async_DeleteFollower()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_DeleteFollower));

            var observer = new User
            {
                UserName = "toma1",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var following = new UserFollowing
            {
                Observer = observer,
                Target = target
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);
                arrangeContext.Followings.Add(following);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                var result = await sut.Delete(target.UserName);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task Return_Correct_Async_DeleteFollowerFail()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_DeleteFollowerFail));

            var observer = new User
            {
                UserName = "toma1",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var following = new UserFollowing
            {
                Observer = observer,
                Target = target
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);
                arrangeContext.Followings.Add(following);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                await Assert.ThrowsExceptionAsync<System.WebException>(async () => await sut.Delete(null));
            }
        }

        [TestMethod]
        public async Task Return_Correct_Async_DeleteFollowerNotFollowing()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_DeleteFollowerNotFollowing));

            var observer = new User
            {
                UserName = "toma1",
                Email = "toma@matrix.com",
                DisplayName = "Toma",
                Bio = "Telerik Akademi Alpha - Gabrovo",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var target = new User
            {
                UserName = "asen",
                DisplayName = "Asen",
                Email = "asen@matrix.com",
                Bio = "Telerik Akademi Alpha - Pavlikeni",
                UserActivities = null,
                Photos = new List<Photo>(),
                Followings = new List<UserFollowing>(),
                Followers = new List<UserFollowing>()
            };

            var loginDto = new LoginDto
            {
                Email = observer.Email,
                Password = observer.PasswordHash
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                arrangeContext.Users.Add(observer);
                arrangeContext.Users.Add(target);

                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<IProfileService> profileService = new Mock<IProfileService>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(observer.UserName);

                var sut = new FollowerService(actContext, userService.Object, profileService.Object);

                await Assert.ThrowsExceptionAsync<System.WebException>(async () => await sut.Delete(target.UserName));
            }
        }
    }
}
