﻿using Matrix.Services.DTO;
using Matrix.Services.DTOs;
using System;
using System.Threading.Tasks;

namespace Matrix.Services.Contracts
{
    /// <summary>
    /// Defines the functionalities of the activity items. 
    /// </summary>
    public interface IActivityService
    {
        Task<ActivityDto> Get(Guid activityId);

        Task<bool> Attend(Guid id);

        Task<bool> UnAttend(Guid id);

        Task<ActivityDto> Update(ActivityDto activityDto);

        Task<ActivityDto> Create(ActivityDto activityDto);

        Task<bool> Delete(Guid activityId);

        Task<ActivityEnvelopeDto> Get(int? limit, int? offset, bool isGoing, bool isHost, DateTime? startDate);
    }
}