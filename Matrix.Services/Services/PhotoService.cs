﻿using Matrix.DataBase;
using Matrix.Models;
using Matrix.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Matrix.Services.Services
{
    /// <summary>
    /// A Photo service class that implements IPhotoService and contains User's photos.
    /// </summary>
    public class PhotoService : Photo, IPhotoService
    {
        #region Fields

        private readonly MatrixDbContext _context;
        private readonly IUserService _userService;
        private readonly ICloudStorageProvider _photoAccessor;

        #endregion

        #region Constructors

        public PhotoService(MatrixDbContext context, IUserService userService, ICloudStorageProvider photoAccessor)
        {
            _context = context;
            _userService = userService;
            _photoAccessor = photoAccessor;
        }

        #endregion

        #region Public Methods

        public async Task<Photo> Create(IFormFile file)
        {
            var photoUploadResult = _photoAccessor.AddPhoto(file);

            var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName ==
            _userService.GetCurrentUserName());

            var photo = new Photo
            {
                Url = photoUploadResult.Url,
                Id = photoUploadResult.PublicId
            };

            if (!user.Photos.Any(x => x.IsMain))
            {
                photo.IsMain = true;
            }

            user.Photos.Add(photo);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return photo;

            throw new Exception("Problem saving changes");
        }

        public async Task<bool> Delete(string id)
        {
            var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName ==
            _userService.GetCurrentUserName());

            var photo = user.Photos.FirstOrDefault(x => x.Id == id);

            if (photo == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { photo = "Not found" });
            }

            if (photo.IsMain == true)
            {
                throw new System.WebException(HttpStatusCode.BadRequest, new { photo = "You cannot delete your profile photo" });
            }

            var result = _photoAccessor.DeletePhoto(photo.Id);

            if (result == null)
            {
                throw new Exception("Problem deleting photo");
            }

            user.Photos.Remove(photo);

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        public async Task<bool> SetMain(string id)
        {
            var user = await _context.Users
                .SingleOrDefaultAsync(x => x.UserName == _userService.GetCurrentUserName());

            var photo = user.Photos.FirstOrDefault(x => x.Id == id);

            if (photo == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { photo = "Not found" });
            }

            var currentMain = user.Photos.FirstOrDefault(x => x.IsMain == true);

            currentMain.IsMain = false;
            photo.IsMain = true;

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        #endregion
    }
}