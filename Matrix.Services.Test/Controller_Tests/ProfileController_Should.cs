﻿using Matrix.Services.Contracts;
using Matrix.Web.ApiControllers;
using Moq;
using NUnit.Framework;
using System;

namespace Matrix.Services.Test.Controllers
{
    [TestFixture]
    public class ProfileController_Should
    {
        private Mock<IProfileService> mockedProfileService;
        private ProfileController profileController;

        [SetUp]
        public void ProfileControllerShouldSetUp()
        {
            mockedProfileService = new Mock<IProfileService>();
            profileController = new ProfileController(mockedProfileService.Object);
        }

        [Test]
        public void TestConstructor()
        {
            Assert.That(profileController, Is.TypeOf<ProfileController>());
            Assert.IsNotNull(profileController);
        }

        [Test]
        public void ThrowsArgumentNullExceptionWhenProfileServiceIsNull()
        {
            mockedProfileService = null;
            Assert.Throws<NullReferenceException>(() => new ProfileController(mockedProfileService.Object));
        }
    }
}