﻿using Matrix.DataBase;
using Matrix.Services.Contracts;
using Matrix.Services.DTO;
using Matrix.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Matrix.Services.Services
{
    /// <summary>
    /// A Profile service class that implements IProfileService and contains User's characteristic.
    /// </summary>
    public class ProfileService : IProfileService
    {
        #region Fields

        private readonly MatrixDbContext _context;
        private readonly IUserService _userService;

        #endregion

        #region Constructors

        public ProfileService(MatrixDbContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        #endregion

        #region Public Methods

        public async Task<ProfileDto> Get(string userName)
        {
            var profile = await ReadProfile(userName);

            return profile;
        }

        public async Task<ProfileDto> ReadProfile(string userName)
        {
            var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == userName);

            if (user == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { User = "Not found" });
            }

            var currentUser = await _context.Users.SingleOrDefaultAsync(x => x.UserName ==
            _userService.GetCurrentUserName());

            var profile = new ProfileDto
            {
                DisplayName = user.DisplayName,
                UserName = user.UserName,
                Image = user.Photos.FirstOrDefault(photo => photo.IsMain)?.Url,
                Photos = user.Photos,
                Bio = user.Bio,
                FollowersCount = user.Followers.Count(),
                FollowingCount = user.Followings.Count()
            };

            if (currentUser.Followings.Any(x => x.TargetId == user.Id))
            {
                profile.IsFollowed = true;
            }

            return profile;
        }

        public async Task<bool> Update(ProfileDto profileDto)
        {
            var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName ==
            _userService.GetCurrentUserName());

            user.DisplayName = profileDto.DisplayName ?? user.DisplayName;
            user.Bio = profileDto.Bio ?? user.Bio;

            var success = await _context.SaveChangesAsync() > 0;

            if (success) return true;

            throw new Exception("Problem saving changes");
        }

        public async Task<ICollection<UserActivityDto>> Get(string userName, string predicate)
        {
            // Lazy Loading
            var user = await _context.Users.SingleOrDefaultAsync(x =>
               x.UserName == userName);

            if (user == null)
            {
                throw new System.WebException(HttpStatusCode.NotFound, new { User = "Not found" });
            }

            var queryable = user.UserActivities
                .OrderBy(a => a.Activity.Date)
                .AsQueryable();

            switch (predicate)
            {
                case "past":
                    queryable = queryable.Where(a => a.Activity.Date <= DateTime.Now);
                    break;
                case "hosting":
                    queryable = queryable.Where(a => a.IsHost == true);
                    break;
                default:
                    queryable = queryable.Where(a => a.Activity.Date >= DateTime.Now);
                    break;
            }

            var activities = queryable.ToList();
            var activitiesToReturn = new List<UserActivityDto>();

            foreach (var activity in activities)
            {
                var userActivityDto = new UserActivityDto
                {
                    Id = activity.Activity.Id,
                    Title = activity.Activity.Title,
                    Category = activity.Activity.Category,
                    Date = activity.Activity.Date
                };

                activitiesToReturn.Add(userActivityDto);
            }

            return activitiesToReturn;
        }

        #endregion
    }
}