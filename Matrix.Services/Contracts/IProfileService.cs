﻿using Matrix.Services.DTO;
using Matrix.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Matrix.Services.Contracts
{
    /// <summary>
    /// Defines the characteristic of the user's profile. 
    /// </summary>
    public interface IProfileService
    {
        Task<ProfileDto> Get(string userName);

        Task<ProfileDto> ReadProfile(string userName);

        Task<bool> Update(ProfileDto profileDto);

        Task<ICollection<UserActivityDto>> Get(string userName, string predicate);
    }
}