﻿using Matrix.Services.Photos;
using Microsoft.AspNetCore.Http;

namespace Matrix.Services.Contracts
{
    public interface ICloudStorageProvider
    {
        UploadResult AddPhoto(IFormFile file);

        string DeletePhoto(string publicId);
    }
}