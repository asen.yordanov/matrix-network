﻿using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Matrix.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        #region Fields

        private readonly IProfileService _profileService;

        #endregion

        #region Constructors

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        #endregion

        #region Public Methods

        [HttpGet("{username}")]
        public async Task<IActionResult> Get(string userName)
        {
            var profile = await _profileService.Get(userName);

            return Ok(profile);
        }

        [HttpPut("")]
        public async Task<IActionResult> Update(ProfileDto profileDto)
        {
            var profile = await _profileService.Update(profileDto);

            return Ok(profile);
        }

        [HttpGet("{username}/activities")]
        public async Task<IActionResult> Get(string userName, string predicate)
        {
            var userActivitiesList = await _profileService.Get(userName, predicate);

            return Ok(userActivitiesList);
        }

        #endregion
    }
}