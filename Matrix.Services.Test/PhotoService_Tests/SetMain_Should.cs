﻿using BeerLab.Services.Test;
using Matrix.DataBase;
using Matrix.Models;
using Matrix.Models.Models;
using Matrix.Services.Contracts;
using Matrix.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matrix.Services.Test.PhotoService_Tests
{
    [TestClass]
    public class SetMain_Should
    {
        [TestMethod]
        public async Task Return_Correct_Async_SetMainPhoto()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Correct_Async_SetMainPhoto));

            var photo1 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "bialg5g7lohxvuknx9na",
                IsMain = true
            };

            var photo2 = new Photo
            {
                Url = "https://res.cloudinary.com/dpc0sub89/image/upload/v1605905678/bialg5g7lohxvuknx9na.jpg",
                Id = "s32d2g7lohxvuknx9na",
                IsMain = false
            };

            var user = new User
            {
                Id = "81bf453e-cdc6-4e07-8771-e4206f082ae4",
                UserName = "toma",
                DisplayName = "Toma",
                Bio = "asdasdasdasdasdasd",
                UserActivities = null,
                Photos = new List<Photo> { photo1, photo2},
                Followings = null,
                Followers = null
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                user.Photos.Add(photo1);
                user.Photos.Add(photo2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<ICloudStorageProvider> photoAccessor = new Mock<ICloudStorageProvider>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(user.UserName);

                var sut = new PhotoService(actContext, userService.Object, photoAccessor.Object);

                photo1.IsMain = false;
                photo2.IsMain = true;

                var result = user.Photos.FirstOrDefault(x => x.IsMain == photo2.IsMain);

                //Assert
                Assert.AreEqual(result.IsMain,photo2.IsMain);
            }
        }

        [TestMethod]
        public async Task Return_Null_Async_SetMainPhoto()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Return_Null_Async_SetMainPhoto));

            var photo1 = new Photo
            {
                Url = null,
                Id = null,
            };

            var photo2 = new Photo
            {
                Url = null,
                Id = null,
            };

            var user = new User
            {
                Id = "81bf453e-cdc6-4e07-8771-e4206f082ae4",
                UserName = "toma",
                DisplayName = "Toma",
                Bio = "asdasdasdasdasdasd",
                UserActivities = null,
                Photos = new List<Photo> { photo1, photo2 },
                Followings = null,
                Followers = null
            };

            await using (var arrangeContext = new MatrixDbContext(options))
            {
                user.Photos.Add(photo1);
                user.Photos.Add(photo2);
                await arrangeContext.SaveChangesAsync();
            }

            //Act
            await using (var actContext = new MatrixDbContext(options))
            {
                Mock<IUserService> userService = new Mock<IUserService>();
                Mock<ICloudStorageProvider> photoAccessor = new Mock<ICloudStorageProvider>();

                userService.Setup(x => x.GetCurrentUserName()).Returns(user.UserName);

                var sut = new PhotoService(actContext, userService.Object, photoAccessor.Object);

                var result = user.Photos.FirstOrDefault(x => x.Id == photo2.Id);

                //Assert
                Assert.IsNull(result.Id);
            }
        }
    }
}
