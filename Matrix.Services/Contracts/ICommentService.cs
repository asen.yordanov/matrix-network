﻿using Matrix.Services.DTOs;
using System.Threading.Tasks;

namespace Matrix.Services.Contracts
{
    /// <summary>
    /// Defines the creation of a comment. 
    /// </summary>
    public interface ICommentService
    {
        Task<CommentDto> Create(CommentDto commentDto);
    }
}