﻿using System;
using System.Net;

namespace Matrix.Services.System
{
    public class WebException : Exception
    {
        public WebException(HttpStatusCode code, object error = null)
        {
            Code = code;
            Error = error;
        }

        public HttpStatusCode Code { get; }

        public object Error { get; }
    }
}