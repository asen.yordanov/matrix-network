﻿using Matrix.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Matrix.Services.Contracts
{
    /// <summary>
    /// Defines the functionalities of the follower members. 
    /// </summary>
    public interface IFollowerService
    {
        Task<bool> Add(string username);

        Task<bool> Delete(string username);

        Task<List<ProfileDto>> Get(string username, string predicate);
    }
}