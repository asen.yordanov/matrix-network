﻿using Matrix.Services.Contracts;
using Matrix.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Matrix.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Fields

        private readonly IUserService _userService;

        #endregion

        #region Constructors

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        #region Public Methods


        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginDto model)
        {
            var user = await this._userService.Login(model);

            return Ok(new
            {
                user.DisplayName,
                user.Token,
                user.UserName,
                user.Image
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto model)
        {
            var user = await this._userService.Register(model);

            return Ok(new
            {
                user.DisplayName,
                user.Token,
                user.UserName,
                user.Image
            });
        }

        [HttpGet]
        public async Task<IActionResult> GetCurrentUser()
        {
            var user = await this._userService.GetCurrentUser();

            return Ok(new
            {
                user.DisplayName,
                user.Token,
                user.UserName,
                user.Image
            });
        }

        #endregion
    }
}