﻿using AutoMapper;
using Matrix.Models.Models;
using Matrix.Services.DTOs;
using System.Linq;

namespace Matrix.Services.Mappers
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {
            CreateMap<Activity, ActivityDto>();
            CreateMap<UserActivity, AttendeeDto>()
                .ForMember(d => d.UserName, options => options.MapFrom(s => s.User.UserName))
                .ForMember(d => d.DisplayName, options => options.MapFrom(s => s.User.DisplayName))
                .ForMember(d => d.Image, options => options.MapFrom(s => s.User.Photos.FirstOrDefault(x => x.IsMain).Url));

            CreateMap<Comment, CommentDto>()
                .ForMember(d => d.UserName, options => options.MapFrom(s => s.Author.UserName))
                .ForMember(d => d.DisplayName, options => options.MapFrom(s => s.Author.DisplayName))
                .ForMember(d => d.ActivityId, options => options.MapFrom(s => s.Activity.Id))
                .ForMember(d => d.Image, options => options.MapFrom(s => s.Author.Photos.FirstOrDefault(x =>
                x.IsMain).Url));
        }
    }
}