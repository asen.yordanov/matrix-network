﻿using Matrix.Services.Contracts;
using Matrix.Web.ApiControllers;
using Moq;
using NUnit.Framework;
using System;

namespace Matrix.Services.Test.Controller_Tests
{
    [TestFixture]
    public class UserController_Should
    {
        private Mock<IUserService> mockedUserService;
        private UserController userController;

        [SetUp]
        public void UserControllerShouldSetUp()
        {
            mockedUserService = new Mock<IUserService>();
            userController = new UserController(mockedUserService.Object);
        }

        [Test]
        public void TestConstructor()
        {
            Assert.That(userController, Is.TypeOf<UserController>());
            Assert.IsNotNull(userController);
        }

        [Test]
        public void ThrowsArgumentNullExceptionWhenUserServiceIsNull()
        {
            mockedUserService = null;
            Assert.Throws<NullReferenceException>(() => new UserController(mockedUserService.Object));
        }
    }
}
